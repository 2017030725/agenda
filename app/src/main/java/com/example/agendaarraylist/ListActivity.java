package com.example.agendaarraylist;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {
    private TableLayout tblLista;
    private ArrayList<Contacto> contactos;
    private ArrayList<Contacto> filter;
    private SearchView srcList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        tblLista = (TableLayout) findViewById(R.id.tblLista);
        srcList = (SearchView) findViewById(R.id.menu_search);
        Button btnNuevo = (Button) findViewById(R.id.btnNuevo);

        Bundle bundleObject = getIntent().getExtras();
        contactos = (ArrayList<Contacto>) bundleObject.getSerializable("contactos");
        filter = contactos;
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
        cargarContactos();
    } //Fin onCreate

    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.searchview,menu);

        MenuItem menuItem = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView)menuItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                buscar(newText);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    public void cargarContactos() {
        for (int x=0; x < contactos.size(); x++) {
            Contacto c = new Contacto(contactos.get(x));
            TableRow nRow = new TableRow(ListActivity.this);

            TextView nText = new TextView(ListActivity.this);
            nText.setText(c.getNombre());

            nText.setTextSize(TypedValue.COMPLEX_UNIT_PT, 6);
            nText.setTextColor((c.isFavorito()) ? Color.BLUE : Color.BLACK);
            nRow.addView(nText);

            Button nButton = new Button(ListActivity.this);
            nButton.setText(R.string.accver);
            nButton.setTextSize(TypedValue.COMPLEX_UNIT_PT, 6);
            nButton.setTextColor(Color.BLACK);

            Button dButton = new Button(ListActivity.this);
            dButton.setText("Eliminar");
            dButton.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            dButton.setTextColor(Color.BLACK);

            nButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Contacto c = (Contacto) v.getTag(R.string.contacto_g);
                    Intent i = new Intent();
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("contacto", c);
                    oBundle.putInt("index", Integer.valueOf(v.getTag(R.string.contacto_g_index).toString()));
                    i.putExtras(oBundle);
                    setResult(RESULT_OK,i);
                    finish();
                }
            });

            dButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent();
                    Bundle oBundle = new Bundle();
                    i.putExtra("index",Integer.valueOf(v.getTag(R.string.contacto_g_index).toString()));
                    setResult(RESULT_FIRST_USER,i);
                    finish();
                }
            });
            nButton.setTag(R.string.contacto_g, c);
            nButton.setTag(R.string.contacto_g_index,x);

            dButton.setTag(R.string.contacto_g_index,x);
            nRow.addView(nButton);
            nRow.addView(dButton);
            tblLista.addView(nRow);
        }
    }

    public void buscar(String text){
        ArrayList<Contacto> list = new ArrayList<>();
        for (int x=0;x< filter.size();x++){
            if(filter.get(x).getNombre().toLowerCase().contains(text.toLowerCase())){
                list.add(filter.get(x));
            }
        }
        contactos = list;
        tblLista.removeAllViews();
        cargarContactos();
    }
}
